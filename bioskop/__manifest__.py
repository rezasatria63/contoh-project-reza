# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Aplikasi Bioskop',
    'version': '13.0.1.0.0',
    'category': 'Custom',
    'summary': 'Custom module for Sale.',
    'description': """
            Custom
    """,
    'website': 'https://www.portcities.net',
    'author':'Alvin Adji.',
    'images': [],
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/category_film_views.xml',
        'views/film_views.xml',
        'views/teather_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False
}

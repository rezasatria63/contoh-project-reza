# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Aplikasi Hotel',
    'version': '13.0.1.0.0',
    'category': 'Custom',
    'summary': 'Custom module for Sale.',
    'description': """
            Custom
    """,
    'website': 'https://www.portcities.net',
    'author':'Reza Satria',
    'images': [],
    'depends': ['base','portal'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'data/sequence.xml',
        'data/email_template.xml',
        'views/kamar_views.xml',
        'views/kelas_views.xml',
        'views/booking_kamar_views.xml',
        'wizard/check_kamar_views.xml',
        'wizard/check_transaksi_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False
}

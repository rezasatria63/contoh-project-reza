# -*- coding: utf-8 -*-
from unicodedata import name
from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning, ValidationError
import datetime
class BookingKamar(models.Model):
    """create new model booking kamar"""
    _name = 'booking.kamar'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    # field

    name = fields.Char(string='Kode Booking', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    partner_id = fields.Many2one('res.partner', track_visibility='onchange')
    kamar_id = fields.Many2one('kamar', domain=[('status_kamar', '=', 'available')], track_visibility='onchange')
    check_in = fields.Datetime()
    check_out = fields.Datetime()
    real_check_in = fields.Datetime(track_visibility='onchange')
    real_check_out = fields.Datetime(track_visibility='onchange')
    permintaan_khusus = fields.Char(track_visibility='onchange')
    state = fields.Selection([ ('draft', 'Draft'),
                                ('booked', 'Booked'),
                                ('check_in', 'Check In'),
                                ('check_out', 'Check Out'),
                                ('done', 'Done')
                                ], string='State',default='draft', required=True, track_visibility='onchange')
    
    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('booking.kamar.sequence') or _('New')
        res = super(BookingKamar, self).create(vals)
        return res
    
    def action_book(self):
        for rec in self:
            rec.state = 'booked'
            rec.kamar_id.status_kamar = 'booked'
            mail_to = rec.partner_id.email
            if not mail_to:
                raise Warning('Please set up email for customer first')
            template_id = self.env.ref('hotel.mail_template_hotel_reminder')
            template_id.send_mail(rec.id, force_send=True)
    
    def action_check_in(self):
        for rec in self:
            rec.state = 'check_in'
            rec.kamar_id.status_kamar = 'check_in'
            rec.real_check_in = datetime.datetime.today()
    
    def action_check_out(self):
        for rec in self:
            rec.state = 'check_out'
            rec.kamar_id.status_kamar = 'check_out'
            rec.real_check_out = datetime.datetime.today()
    
    def action_done(self):
        for rec in self:
            rec.state = 'done'
            rec.kamar_id.status_kamar = 'available'
            mail_to = rec.partner_id.email
            if not mail_to:
                raise Warning('Please set up email for customer first')
            template_id = self.env.ref('hotel.mail_template_hotel_thanks')
            template_id.send_mail(rec.id, force_send=True)
    
    def check_kamar(self):
        form_id = self.env.ref('hotel.view_check_kamar_form').id
        return {'name': 'Wizard Check Kamar',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'check.kamar',
                'views': [(form_id, 'form')],
                'target': 'new',
                }
            
    
    
    

    

    

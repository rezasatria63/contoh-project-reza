from odoo import fields, models, _
from odoo.exceptions import Warning

class CheckKamar(models.TransientModel):
    _name = 'check.kamar'

    def check_kamar(self):
        kamar_available = self.env['kamar'].search([('status_kamar', '=', 'available')])
        if kamar_available:
            list_kamar = ''
            for kamar in kamar_available:
                list_kamar+=kamar.name + ','
            raise Warning(list_kamar)
        raise('Tidak ada kamar kosong')
        


from odoo import fields, models, _
from odoo.exceptions import Warning

class CheckTransaksi(models.TransientModel):
    _name = 'check.transaksi'

    #fields
    start_date = fields.Datetime(String='Start Date')
    end_date = fields.Datetime(String='End Date')
    state = fields.Selection([('all', '--All State--'),
                            ('draft', 'Draft'),
                            ('booked', 'Booked'),
                            ('check_in', 'Check In'),
                            ('check_out', 'Check Out'),
                            ('done', 'Done')
                            ], string='State',default='all', required=True)   

    def check_transaksi(self):
        """Transaction Check"""
        transaksi = self.env['booking.kamar'].search([])
        if transaksi:
            list_transaksi = ''
            for trans in transaksi:
                list_transaksi+=trans.name + ' - ' + trans.state + '\n'
            raise Warning(list_transaksi)
        raise('Tidak ada transaksi')
        


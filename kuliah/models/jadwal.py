# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
# from odoo.exceptions import UserError, Warning, ValidationError

class Jadwal(models.Model):
    """create new model jadwal kuliah"""
    _name = 'jadwal'
    
    # field
    mahasiswa_id    = fields.Many2one('mahasiswa')
    matkul_id       = fields.Many2one('mata.kuliah')
    hari            = fields.Selection([('senin', 'Senin'),
                                        ('selasa', 'Selasa'),
                                        ('rabu', 'Rabu'),
                                        ('kamis', 'Kamis'),
                                        ('jumat', 'Jumat'),
                                        ], string='Hari', required=True)
    jam_mulai       = fields.Float()
    jam_selesai     = fields.Float()

    

    

    

# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
# from odoo.exceptions import UserError, Warning, ValidationError

class Jurusan(models.Model):
    """create new model jurusan"""
    _name = 'jurusan'
    
    # field

    name = fields.Char()
    kode_jurusan = fields.Char()
    
    def action_shortcut_jurusan(self):
        """open editable tree"""
        self.ensure_one()
        form_id = self.env.ref('kuliah.view_jurusan_form').id
        return {'name': 'Jurusan',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'jurusan',
                'views': [(form_id, 'form')],
                'target': 'current',
                'res_id': self.id
                }
    

    

    
